const dbConnection = require("../dbConnection");
const queries = require("../queries/queries");

module.exports = class GoldPriceDao {
    async readEntities() {
    let con = await dbConnection();
    try {
      await con.query("START TRANSACTION");
      let goldprice = await con.query(queries.read_snp_currentspot);
      await con.query("COMMIT");
      goldprice = JSON.parse(JSON.stringify(goldprice));
      return goldprice;
    } catch (ex) {
      console.log(ex);
      throw ex;
    } finally {
      await con.release();
      await con.destroy();
    }
  }

  async readGoldCalc(){
     let con = await dbConnection();
    try {
      await con.query("START TRANSACTION");
      let result = await con.query(queries.read_GoldCalc);
      await con.query("COMMIT");
      result = JSON.parse(JSON.stringify(result));
      return result;
    } catch (ex) {
      console.log(ex);
      throw ex;
    } finally {
      await con.release();
      await con.destroy();
    }
  }

  async readPriceMargin(){
     let con = await dbConnection();
    try {
      await con.query("START TRANSACTION");
      let result = await con.query(queries.read_PriceMargin);
      await con.query("COMMIT");
      result = JSON.parse(JSON.stringify(result));
      return result;
    } catch (ex) {
      console.log(ex);
      throw ex;
    } finally {
      await con.release();
      await con.destroy();
    }
  }
}