const dbConnection = require("../dbConnection");
const queries = require("../queries/queries");

module.exports = class OrderDao {
  async readEntities() {
    // let con = await dbConnection();
    // try {
    //   await con.query("START TRANSACTION");
    //   let goldprice = await con.query(queries.read_snp_currentspot);
    //   await con.query("COMMIT");
    //   goldprice = JSON.parse(JSON.stringify(goldprice));
    //   return goldprice;
    // } catch (ex) {
    //   console.log(ex);
    //   throw ex;
    // } finally {
    //   await con.release();
    //   await con.destroy();
    // }
  }

  async createOrder(entity) {
    let con = await dbConnection();
    try {
      var today = new Date();
      var curno =
        today.getYear().toString().substr(-2) +
        ("0" + (today.getMonth() + 1)).slice(-2) +
        ("0" + today.getDate()).slice(-2) +
        "0001";
      let res = await con.query(queries.OnlineTransaction_getOrderNumber, [
        curno,
        curno,
      ]);
      entity.order_no = res[0].runno;
      entity.order_date = today;
      //console.log(entity);
      await con.query("START TRANSACTION");
      await con.query(queries.OnlineTransaction_updateOrderNumber, [
        entity.order_no,
      ]);
      await con.query(queries.OnlineTransaction_insertHeader, [
        entity.order_no,
        entity.order_date,
        entity.customer_code,
        entity.order_type,
        entity.order_gross,
        entity.order_net,
        entity.order_netvat,
        entity.receive_method,
        entity.pay_method,
        entity.laststatus,
      ]);
      await con.query(queries.OnlineTransaction_insertDetail, [
        entity.order_no,
        entity.order_lineno,
        entity.productcode,
        entity.order_qty,
        entity.unitcode,
        entity.order_unitprice,
        entity.order_gross,
        entity.order_net,
        entity.order_netvat,
        entity.currentunitprice,
        entity.currentExchange,
        entity.currentSpot,
        entity.spotInclude,
        entity.convertRate,
        entity.pricemarginamt,
      ]);
      await con.query("COMMIT");
      //let result = '{"result":"Completed","order_no" : '+entity.order_no+'}';
      let result = {
        result: "Completed",
        order_no: entity.order_no,
        order_date: entity.order_date,
      };
      //console.log(result);
      return result; // "{result : 'Completed',order_no: "+entity.order_no+"}";
    } catch (ex) {
      console.log(ex);
      await con.query("rollback");
    } finally {
    }
  }

  async createBooking(entity) {
    let con = await dbConnection();
    try {
      var today = new Date();
      var curno =
        today.getYear().toString().substr(-2) +
        ("0" + (today.getMonth() + 1)).slice(-2) +
        ("0" + today.getDate()).slice(-2) +
        "0001";
      let res = await con.query(queries.OnlineTransaction_getBookingNumber, [
        curno,
        curno,
      ]);
      entity.order_no = res[0].runno;
      entity.order_date = today;
      //console.log(entity);
      await con.query("START TRANSACTION");
      await con.query(queries.OnlineTransaction_updateBookingNumber, [
        entity.order_no,
      ]);
      await con.query(queries.OnlineTransaction_insertBookingHeader, [
        entity.order_no,
        entity.order_date,
        entity.customer_code,
        entity.order_type,
        entity.order_gross,
        entity.order_net,
        entity.order_netvat,
        entity.order_noref,
        entity.laststatus,
      ]);
      await con.query(queries.OnlineTransaction_insertBookingDetail, [
        entity.order_no,
        entity.order_lineno,
        entity.productcode,
        entity.pricecondition,
        entity.order_qty,
        entity.unitcode,
        entity.order_unitpriceresv,
        entity.currentunitprice,
        entity.currentExchange,
        entity.currentSpot,
        entity.spotInclude,
        entity.convertRate,
        entity.pricemarginamt,
        entity.order_gross,
        entity.order_net,
        entity.order_netvat,
      ]);
      await con.query("COMMIT");
      //let result = '{"result":"Completed","order_no" : '+entity.order_no+'}';
      let result = {
        result: "Completed",
        order_no: entity.order_no,
        order_date: entity.order_date,
      };
      //console.log(result);
      return result; // "{result : 'Completed',order_no: "+entity.order_no+"}";
    } catch (ex) {
      console.log(ex);
      await con.query("rollback");
    } finally {
    }
  }

  async cancelBooking(entity) {
    let con = await dbConnection();
    try {
      
      await con.query("START TRANSACTION");
      await con.query(queries.booking_cancel_moveDetailtohist, [
        entity.order_no,
      ]);
      await con.query(queries.booking_cancel_moveHeadtohist, [entity.username,
        entity.order_no,
      ]);
      await con.query(queries.booking_cancel_delDetail, [
        entity.order_no,
      ]);
      await con.query(queries.booking_cancel_delHead, [
        entity.order_no,
      ]);
      await con.query("COMMIT");
      //let result = '{"result":"Completed","order_no" : '+entity.order_no+'}';
      let result = {
        result: "Completed",
        order_no: entity.order_no,
      };
      //console.log(result);
      return result; // "{result : 'Completed',order_no: "+entity.order_no+"}";
    } catch (ex) {
      console.log(ex);
      await con.query("rollback");
    } finally {
    }
  }

  async getOrder(entity) {
    let con = await dbConnection();
    try {
      let result = {};
      let qr = "";
      if (entity.docstate == 10) qr = queries.order_query_getData;
      else qr = queries.order_query_getDataFromHist;
      let results = await con.query(qr, [
        entity.customercode,
        entity.startdate,
        entity.enddate,
        entity.doctype,
        entity.doctype,
      ]);

      results = JSON.parse(JSON.stringify(results));
      return results;
    } catch (ex) {
      throw ex;
    } finally {
      await con.release();
      await con.destroy();
    }
  }

  async getBooking(entity) {
    let con = await dbConnection();
    try {
      let result = {};
      let qr = queries.booking_query_getData;
      if (entity.docstate == "10") entity.docstate = "รายการใหม่";
      else if (entity.docstate == "80") entity.docstate = "ดำเนินการแล้ว";
      else entity.docstate = "ยกเลิก";
      let results = await con.query(qr, [
        entity.customercode,
        entity.startdate,
        entity.enddate,
        entity.doctype,
        entity.doctype,
        entity.docstate,
      ]);
      results = JSON.parse(JSON.stringify(results));
      return results;
    } catch (ex) {
      throw ex;
    } finally {
      await con.release();
      await con.destroy();
    }
  }

  async summaryBuyAndSales(entity){
    let con = await dbConnection();
    try {
      let result = {};
      let qr = queries.dashboard_summaryBuyandSales;
     
      let results = await con.query(qr, [
        entity.customercode
      ]);
      results = JSON.parse(JSON.stringify(results));
      return results;
    } catch (ex) {
      throw ex;
    } finally {
      await con.release();
      await con.destroy();
    }
  }

  async summaryBuyAndSales_byMonth(entity){
    let con = await dbConnection();
    try {
      let result = {};
      let qr = queries.dashboard_summaryBuyandSales_byMonth;
     
      let results = await con.query(qr, [
        entity.customercode
      ]);
      results = JSON.parse(JSON.stringify(results));
      return results;
    } catch (ex) {
      throw ex;
    } finally {
      await con.release();
      await con.destroy();
    }
  }

  async PriceMinMax(){
    let con = await dbConnection();
    try {
      let result = {};
      let qr = queries.dashboard_price_minmax;
     console.log(qr);
      let results = await con.query(qr);
      results = JSON.parse(JSON.stringify(results));
      return results;
    } catch (ex) {
      throw ex;
    } finally {
      await con.release();
      await con.destroy();
    }
  }

  async spot_maxdaily(){
    let con = await dbConnection();
    try {
      let result = {};
      let qr = queries.dashboard_spot_maxdaily;
     
      let results = await con.query(qr);
      results = JSON.parse(JSON.stringify(results));
      return results;
    } catch (ex) {
      throw ex;
    } finally {
      await con.release();
      await con.destroy();
    }
  }

  async AvgSpotByDay(){
    let con = await dbConnection();
    try {
      let result = {};
      let qr = queries.dashboard_AvgSpotByDay;
     
      let results = await con.query(qr);
      results = JSON.parse(JSON.stringify(results));
      return results;
    } catch (ex) {
      throw ex;
    } finally {
      await con.release();
      await con.destroy();
    }
  }

  async SalesAndBuyAssessment(entity){
    let con = await dbConnection();
    try {
      let result = {};
      let qr = queries.SalesAndBuyAssessment;
      //console.log(entity);
     
      let results = await con.query(qr,[entity.customercode]);
      results = JSON.parse(JSON.stringify(results));
      return results;
    } catch (ex) {
      throw ex;
    } finally {
      await con.release();
      await con.destroy();
    }
  }
};
