const dbConnection = require("../dbConnection");
const queries = require("../queries/queries");

module.exports = class CustomerDao {

    async readEntities(entity) {
    let con = await dbConnection();
    console.log('test')
    try {
      //await con.query("START TRANSACTION");
      let results = await con.query(queries.read_customer,[entity.username,entity.username]);
      //await con.query("COMMIT");
      results = JSON.parse(JSON.stringify(results));
      return results;
    } catch (ex) {
      console.log(ex);
      throw ex;
    } finally {
      await con.release();
      await con.destroy();
    }
  }
  
  async readCustomerCredit(entity) {
    let con = await dbConnection();
    try {
      await con.query("START TRANSACTION");
      let results = await con.query(queries.read_customer_CustomerCredit,[entity.type,entity.type,entity.username]);
      await con.query("COMMIT");
      results = JSON.parse(JSON.stringify(results));
      //let results =  queries.read_customer_CustomerCredit;
      return results;
    } catch (ex) {
      console.log(ex);
      throw ex;
    } finally {
      await con.release();
      await con.destroy();
    }
  }

  async checkPinCode(entity) {
    console.log(entity.pincode);
    let con = await dbConnection();
    try {
      let results = await con.query(queries.customer_checkPinCode,[entity.username,entity.pincode]);
      results = JSON.parse(JSON.stringify(results));
      return results;
    } catch (ex) {
      console.log(ex);
      throw ex;
    } finally {
      await con.release();
      await con.destroy();
    }
  }



   

}