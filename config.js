const port = "4011"
const secretKey = "secretKey"
const expiredAfter = 60 * 60 * 1000
const uploadPath = "/var/www/p-tax.net/html/public/upload"
export default {
  port,
  secretKey,
  expiredAfter,
  uploadPath,
}
