module.exports = {
  insert_customer: "INSERT INTO tbl_todo(title, completed) VALUES(?, ?)",
  read_customer: "SELECT *,concat(Address1,' ',Address2,' ',Address3,' ',Address4,' ',ZipCode) as fullAddress "
    +" FROM snp_customer where username =? or ? = 'all'",
  update_customer:
    "UPDATE tbl_todo SET tbl_todo.title = ?, tbl_todo.completed = ? WHERE tbl_todo.id = ?",
  delete_customer: "DELETE FROM tbl_todo WHERE tbl_todo.id = ?",
  read_snp_currentspot:
    "SELECT spotupdatetime,case when enable_spot_manual ='Y' then spot_bid_manual else spot_bid end as spot_bid" +
    ",case when enable_spot_manual ='Y' then spot_ask_manual else spot_ask end as spot_ask " +
    ",case when allowusefix = 'N' then currencyRate else currencyratefix end as currencyRate ,spot_status,iszero,zerodatetime,currencyratefix,currencyRate as currencyrateauto,currencyupdatetime " +
    ",enable_spot_manual,spot_bid_manual,spot_ask_manual " +
    " FROM snp_currentspot " +
    " left join snp_fix_currency on 1=1 " +
    " left join snp_spot_manual on 1=1",
  read_GoldCalc:
    "select configname,configvalue from snp_program_config where configgroup = 'GoldCalc'",
  read_PriceMargin:
    "select productcode,salepricemargin,buypricemargin,salepricemargin_amt,buypricemargin_amt from snp_product_margin",
    //==========================================================
   
  read_customer_CustomerCredit:
    "SELECT cst.customer_code, AllowValue1, AllowValue2, AllowValue3, deposit, sum( pdmar.stepprice_perunit * COALESCE(od.order_qty ,0) ) AS sum_amount" +
    ", deposit - sum( COALESCE(pdmar.stepprice_perunit,0) * COALESCE(od.order_qty ,0) ) AS deposit_remain,allowvalue1 - sum( COALESCE(pdmar.stepprice_perunit,0) * COALESCE(od.order_qty ,0) ) AS allowvalue1_remain" +
    ",FLOOR((allowvalue2*configvalue - sum( COALESCE(pdmar.stepprice_perunit,0) * COALESCE(od.order_qty ,0) ))/configvalue) AS allowvalue2_remain" +
    ",FLOOR((allowvalue3*configvalue - sum( COALESCE(pdmar.stepprice_perunit,0) * COALESCE(od.order_qty ,0) ))/configvalue) AS allowvalue3_remain" +
    " FROM snp_customer cst LEFT JOIN (SELECT oh.customer_code,od.productcode, od.order_qty - od.order_clear_qty as order_qty" +
    " FROM snp_order_header oh INNER JOIN snp_order_detail od ON oh.order_no = od.order_no WHERE oh.order_type = ?" +
    " UNION ALL" +
    " SELECT ORH.customer_code,ORD.PRODUCTCODE, ORD.order_qty" +
    " FROM snp_orderreserve_header ORH" +
    " INNER JOIN snp_orderreserve_detail ORD ON ORH.order_no = ORD.order_no" +
    " WHERE ORH.laststatus =10 and ORH.order_type = ?" +
    " ) od ON od.customer_code = cst.customer_code" +
    " LEFT JOIN snp_product_margin pdmar ON pdmar.productcode = od.productcode " +
    " left join snp_program_config on configgroup = 'goldcalc'" +
    " and configname = 'step2Ratio'" +
    " where cst.username = ? " +
    " GROUP BY cst.customer_code, AllowValue1, AllowValue2, AllowValue3, deposit",
  //====================================================
  OnlineTransaction_getOrderNumber : "select (case when running_value < ? then ? else running_value+1 end) as runno  from snp_runningno where running_group  = 'order' and running_code = 'ordno' and activestatus = 'Y'",
  OnlineTransaction_updateOrderNumber : "update snp_runningno set running_value = ? where running_group  = 'order' and running_code = 'ordno' and activestatus = 'Y'",
  OnlineTransaction_insertHeader: "insert into snp_order_header(order_no,order_date,customer_code,order_type,order_gross,order_net, order_netvat,receive_method,pay_method,laststatus) values(?,?,?,?,?,?,?,?,?,?)",
  //OnlineTransaction_insertDetail: "insert into snp_order_detail(order_no,order_lineno,productcode,order_qty,unitcode,order_unitprice, order_gross,order_net,order_netvat,currentunitprice,currentExchange,currentSpot,spotInclude,convertRate,pricemarginamt) values(?,?,?,?,?,?,?,?,?,?)",
  OnlineTransaction_insertDetail: "insert into snp_order_detail(order_no,order_lineno,productcode,order_qty,unitcode,order_unitprice, order_gross,order_net,order_netvat,currentunitprice,currentExchange,currentSpot,spotInclude,convertRate,pricemarginamt) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
  OnlineTransaction_checktime: "select case when Time_To_Sec(TIMEDIFF(localtime(),max(order_date))) is null then 100 else Time_To_Sec(TIMEDIFF(localtime(),max(order_date))) end as difftime from snp_order_header where customer_code =?",
  OnlineTransaction_getRunNo: "",
  OnlineTransaction_updateRunNo: "",
  //===============================================
  //==================================================booking===========================
  OnlineTransaction_getBookingNumber : "select (case when running_value < ? then ? else running_value+1 end) as runno  from snp_runningno where running_group  = 'order' and running_code = 'rsvno' and activestatus = 'Y'",
  OnlineTransaction_updateBookingNumber : "update snp_runningno set running_value = ? where running_group  = 'order' and running_code = 'rsvno' and activestatus = 'Y'",
  OnlineTransaction_insertBookingHeader: "insert into snp_orderreserve_header(order_no,order_date,customer_code,order_type,order_gross,order_net, order_netvat,order_noref,laststatus) values(?,?,?,?,?,?,?,?,?)",
  OnlineTransaction_insertBookingDetail: "insert into snp_orderreserve_detail(order_no,order_lineno,productcode,pricecondition,order_qty,unitcode,order_unitpriceresv,currentunitprice,currentExchange,currentSpot,spotInclude,convertRate,pricemarginamt,order_gross,order_net,order_netvat) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
  //====================================================================================
  order_query_getData : "select * from" +
" (SELECT oh.customer_code,oh.order_no,oh.order_date,oh.order_type" +
	",case when oh.order_type = 'O' then 'ขาย' else 'ซื้อ' end as order_type_name" +
	",od.productcode,od.order_qty- (case when od.order_clear_qty is null then 0 else od.order_clear_qty  end) as order_qty,od.unitcode,od.order_unitprice,od.order_netvat - (case when od.order_clear_netvat is null then 0 else od.order_clear_netvat  end) as order_netvat" +
	",oh.receive_status,oh.payment_status" +
	" ,case oh.laststatus when 10 then 'รายการใหม่' when 80 then 'รายการสมบูรณ์'" +
	" end as laststatus,case when order_method is null or order_method = '' then 'A' else 'B' end as order_method " +
" FROM snp_order_header oh inner join snp_order_detail od" +
"	on oh.order_no = od.order_no where od.order_qty- (case when od.order_clear_qty is null then 0 else od.order_clear_qty  end) > 0" +
" union all" +
" SELECT oh.customer_code,case when payment_no > 0 then concat(cast(oh.order_no as char(10)),'-',cast(oh.payment_no as char(2))) else oh.order_no end as order_no,oh.order_date,oh.order_type" +
"	,case when oh.order_type = 'O' then 'ขาย' else 'ซื้อ' end as order_type_name" +
"	,od.productcode,od.order_qty,od.unitcode,od.order_unitprice,od.order_netvat	,oh.receive_status,oh.payment_status" +
"	,case oh.laststatus when 10 then 'รายการใหม่' when 80 then 'รายการสมบูรณ์'" +
"	end as laststatus,case when order_method is null or order_method = '' then 'A' else 'B' end as order_method " +
" FROM snp_payment_header oh inner join snp_payment_detail od" +
"	on oh.receive_no = od.receive_no)" +
" as a where customer_code = ? and order_date >= ? and order_date <= ? and (order_type = ? or ? = 'all') ",
order_query_getDataFromHist : " select * from "+
" (SELECT oh.customer_code,case when payment_no > 0 then concat(cast(oh.order_no as char(10)),'-',cast(oh.payment_no as char(2))) else oh.order_no end as order_no,oh.order_date,oh.order_type "+
"	,case when oh.order_type = 'O' then 'ขาย' else 'ซื้อ' end as order_type_name "+
"		,od.productcode,od.order_qty,od.unitcode,od.order_unitprice,od.order_netvat	,oh.receive_status,oh.payment_status "+
"		,case oh.laststatus when 10 then 'รายการใหม่' when 80 then 'รายการสมบูรณ์' "+
"		end as laststatus,case when order_method is null or order_method = '' then 'A' else 'B' end as order_method  "+
"	FROM snp_payment_header_hist oh inner join snp_payment_detail_hist od "+
"		on oh.receive_no = od.receive_no) "+
"	as a where customer_code = ? and order_date >= ? and order_date <= ? and (order_type = ? or ? = 'all') ",
booking_query_getData : "SELECT * FROM "+
" ( SELECT oh.customer_code,oh.order_no,oh.order_date "+
",case when oh.order_type = 'O' then 'ขาย' else 'ซื้อ' end as order_type"+
",od.productcode,od.order_qty,od.unitcode,currentSpot,od.order_unitpriceresv ,od.order_netvat"+
",case oh.laststatus when 10 then 'รายการใหม่' when 80 then 'ดำเนินการแล้ว' when 90 then 'ยกเลิก'"+
" end as laststatus FROM snp_orderreserve_header oh inner join snp_orderreserve_detail od"+
" on oh.order_no = od.order_no "+
" UNION ALL"+
" SELECT oh.customer_code,oh.order_no,oh.order_date"+
",case when oh.order_type = 'O' then 'ขาย' else 'ซื้อ' end as order_type"+
",od.productcode,od.order_qty,od.unitcode,currentSpot,od.order_unitpriceresv ,od.order_netvat"+
",case oh.laststatus when 10 then 'รายการใหม่' when 80 then 'ดำเนินการแล้ว' when 90 then 'ยกเลิก'"+
" end as laststatus FROM snp_orderreserve_header_hist oh inner join snp_orderreserve_detail_hist od"+
" on oh.order_no = od.order_no ) as a where customer_code = ? and order_date >= ? and order_date <= ? and (order_type = ? or ? = 'all') and laststatus = ?",
customer_checkPinCode : "select pincode from snp_customer where username = ? and pincode = ?",
booking_cancel_moveDetailtohist : "insert into snp_orderreserve_detail_hist select * from snp_orderreserve_detail where order_no  = ?",
booking_cancel_moveHeadtohist :"insert into snp_orderreserve_header_hist select order_no, order_date, customer_code, order_type, order_gross, order_net, order_netvat, order_noref,90,'SNPADMIN',?,curdate() from snp_orderreserve_header where order_no  = ?",
booking_cancel_delDetail : "delete from snp_orderreserve_detail where order_no  = ?",
booking_cancel_delHead : "delete from snp_orderreserve_header where order_no  = ?",
dashboard_summaryBuyandSales : "SELECT sum(case when order_type = 'B' then order_netvat else 0 end) as order_netvat_buy "+
",sum(case when order_type = 'O' then order_netvat else 0 end) as order_netvat_sales "+
" FROM snp_order_header  "+
" where customer_code = ? and year(order_date) = year(CURRENT_DATE) and month(order_date) = month(CURRENT_DATE) " ,
dashboard_summaryBuyandSales_byMonth : " SELECT year(order_date) as yr, month(order_date) as mth, sum(case when order_type = 'B' then order_netvat else 0 end) as order_netvat_buy "+
",sum(case when order_type = 'O' then order_netvat else 0 end) as order_netvat_sales "+
" FROM snp_order_header  "+
" where customer_code = ? and year(order_date) = year(CURRENT_DATE)  "+
" group by year(order_date) , month(order_date) ",
dashboard_price_minmax : "select  'TODAY' AS DATA_DAY,'BID965' AS MAXTYPE, DATE_FORMAT(min(lastdatetime),'%H:%i:%s') AS TIME ,MAX(BAHT_BID965) AS BAHT_PRICE  "+
" from snp_bahtprice_hist a  "+
" inner join (  "+
" 	select  max(Baht_bid965) as max_baht_bid965  "+
" 	from snp_bahtprice_hist where year(lastdatetime) = year(now()) and  "+
" month(lastdatetime) = month(now()) and day(lastdatetime) = day(now()) and  onlinestatus > 1  "+
" ) b on a.baht_bid965 = b.max_baht_bid965  "+
" where year(a.lastdatetime) = year(now()) and month(a.lastdatetime) = month(now()) and day(lastdatetime) = day(now()) and  a.onlinestatus > 1  "+
" UNION ALL select 'TODAY' AS DATA_DAY,'ASK965' AS MAXTYPE, DATE_FORMAT(min(lastdatetime),'%H:%i:%s') AS MIN_TIME_ASK965 ,MIN(Baht_ask965 ) AS MIN_BAHT_ASK965  from snp_bahtprice_hist a inner join ( select  MIN(Baht_ASK965) as MIN_baht_ASK965 from snp_bahtprice_hist where year(lastdatetime) = year(now()) and month(lastdatetime) = month(now()) and day(lastdatetime) = day(now()) and  onlinestatus > 1 ) b on a.baht_ASK965 = b.mIN_baht_ASK965 where year(a.lastdatetime) = year(now()) and month(a.lastdatetime) = month(now()) and day(a.lastdatetime) = day(now()) and  a.onlinestatus > 1  "+
" UNION ALL select 'TODAY' AS DATA_DAY,'BID999' AS MAXTYPE,DATE_FORMAT(min(lastdatetime),'%H:%i:%s') AS MAX_TIME_BID999,MAX(BAHT_BID999) AS MAX_BAHT_BID999  from snp_bahtprice_hist a inner join ( select  max(Baht_bid999) as max_baht_bid999 from snp_bahtprice_hist where year(lastdatetime) = year(now()) and month(lastdatetime) = month(now()) and day(lastdatetime) = day(now()) and  onlinestatus > 1 ) b on a.baht_bid999 = b.max_baht_bid999 where year(a.lastdatetime) = year(now()) and month(a.lastdatetime) = month(now()) and day(a.lastdatetime) = day(now()) and  a.onlinestatus > 1  "+
" UNION ALL select  'TODAY' AS DATA_DAY,'ASK999' AS MAXTYPE,DATE_FORMAT(min(lastdatetime),'%H:%i:%s') AS MIN_TIME_ASK999 ,MIN(Baht_ask999 ) AS MIN_BAHT_ASK999  from snp_bahtprice_hist a inner join ( select  MIN(Baht_ASK999) as MIN_baht_ASK999 from snp_bahtprice_hist where year(lastdatetime) = year(now()) and month(lastdatetime) = month(now()) and day(lastdatetime) = day(now()) and  onlinestatus > 1 ) b on a.baht_ASK999 = b.mIN_baht_ASK999 where year(a.lastdatetime) = year(now()) and month(a.lastdatetime) = month(now()) and day(a.lastdatetime) = day(now()) and  a.onlinestatus > 1 "+
" UNION ALL "+
" select  'YESTERDAY' AS DATA_DAY,'BID965' AS MAXTYPE, DATE_FORMAT(min(lastdatetime),'%H:%i:%s') AS TIME ,MAX(BAHT_BID965) AS BAHT_PRICE   "+
" from snp_bahtprice_hist a  "+
" inner join (  "+
" 	select  max(Baht_bid965) as max_baht_bid965  "+
" 	from snp_bahtprice_hist where year(lastdatetime) = year(DATE_SUB(now(),INTERVAL 1 day)) and  "+
" month(lastdatetime) = month(DATE_SUB(now(),INTERVAL 1 day)) and day(lastdatetime) = day(DATE_SUB(now(),INTERVAL 1 day)) and  onlinestatus > 1  "+
" ) b on a.baht_bid965 = b.max_baht_bid965  "+
" where year(a.lastdatetime) = year(DATE_SUB(now(),INTERVAL 1 day))  "+
" and month(a.lastdatetime) = month(DATE_SUB(now(),INTERVAL 1 day))   "+
" and day(a.lastdatetime) = day(DATE_SUB(now(),INTERVAL 1 day))  "+
" and  a.onlinestatus > 1  "+
" UNION ALL select 'YESTERDAY' AS DATA_DAY,'ASK965' AS MAXTYPE, DATE_FORMAT(min(lastdatetime),'%H:%i:%s') AS MIN_TIME_ASK965 ,MIN(Baht_ask965 ) AS MIN_BAHT_ASK965  from snp_bahtprice_hist a  "+
" inner join ( select  MIN(Baht_ASK965) as MIN_baht_ASK965 from snp_bahtprice_hist where year(lastdatetime) = year(DATE_SUB(now(),INTERVAL 1 day)) and month(lastdatetime) = month(DATE_SUB(now(),INTERVAL 1 day)) and day(lastdatetime) = day(DATE_SUB(now(),INTERVAL 1 day)) and  onlinestatus > 1 ) b on a.baht_ASK965 = b.mIN_baht_ASK965  "+
" where year(a.lastdatetime) = year(DATE_SUB(now(),INTERVAL 1 day)) and month(a.lastdatetime) = month(DATE_SUB(now(),INTERVAL 1 day)) and day(a.lastdatetime) = day(DATE_SUB(now(),INTERVAL 1 day)) and  a.onlinestatus > 1  "+
" UNION ALL select 'YESTERDAY' AS DATA_DAY,'BID999' AS MAXTYPE,DATE_FORMAT(min(lastdatetime),'%H:%i:%s') AS MAX_TIME_BID999,MAX(BAHT_BID999) AS MAX_BAHT_BID999  from snp_bahtprice_hist a inner join ( select  max(Baht_bid999) as max_baht_bid999 from snp_bahtprice_hist where year(lastdatetime) = year(DATE_SUB(now(),INTERVAL 1 day)) and month(lastdatetime) = month(DATE_SUB(now(),INTERVAL 1 day)) and day(lastdatetime) = day(DATE_SUB(now(),INTERVAL 1 day)) and  onlinestatus > 1 ) b on a.baht_bid999 = b.max_baht_bid999 where year(a.lastdatetime) = year(DATE_SUB(now(),INTERVAL 1 day)) and month(a.lastdatetime) = month(DATE_SUB(now(),INTERVAL 1 day)) and day(a.lastdatetime) = day(DATE_SUB(now(),INTERVAL 1 day)) and  a.onlinestatus > 1  "+
" UNION ALL select  'YESTERDAY' AS DATA_DAY,'ASK999' AS MAXTYPE,DATE_FORMAT(min(lastdatetime),'%H:%i:%s') AS MIN_TIME_ASK999 ,MIN(Baht_ask999 ) AS MIN_BAHT_ASK999  from snp_bahtprice_hist a inner join ( select  MIN(Baht_ASK999) as MIN_baht_ASK999 from snp_bahtprice_hist where year(lastdatetime) = year(DATE_SUB(now(),INTERVAL 1 day)) and month(lastdatetime) = month(DATE_SUB(now(),INTERVAL 1 day)) and day(lastdatetime) = day(DATE_SUB(now(),INTERVAL 1 day)) and  onlinestatus > 1 ) b on a.baht_ASK999 = b.mIN_baht_ASK999 where year(a.lastdatetime) = year(DATE_SUB(now(),INTERVAL 1 day)) and month(a.lastdatetime) = month(DATE_SUB(now(),INTERVAL 1 day)) and day(a.lastdatetime) = day(DATE_SUB(now(),INTERVAL 1 day)) and  a.onlinestatus > 1",
// dashboard_price_minmax : "select 'BID965' AS MAXTYPE, DATE_FORMAT(min(lastdatetime),'%H:%i:%s') AS TIME ,MAX(BAHT_BID965) AS BAHT_PRICE  from snp_bahtprice_hist a "+
// "inner join ( "+
// "select  max(Baht_bid965) as max_baht_bid965 from snp_bahtprice_hist "+
// "where year(lastdatetime) = year(now()) "+
// "and month(lastdatetime) = month(now()) "+
// "and day(lastdatetime) = day(now()) "+
// "and  onlinestatus > 1 "+
// ") b on a.baht_bid965 = b.max_baht_bid965 "+
// "where year(a.lastdatetime) = year(now()) "+
// "and month(a.lastdatetime) = month(now()) "+
// "and day(lastdatetime) = day(now()) "+
// "and  a.onlinestatus > 1 "+
// "UNION ALL "+
// "select 'ASK965' AS MAXTYPE, DATE_FORMAT(min(lastdatetime),'%H:%i:%s') AS MIN_TIME_ASK965 ,MIN(Baht_ask965 ) AS MIN_BAHT_ASK965  from snp_bahtprice_hist a "+
// "inner join ( "+
// "select  MIN(Baht_ASK965) as MIN_baht_ASK965 from snp_bahtprice_hist "+
// "where year(lastdatetime) = year(now()) "+
// "and month(lastdatetime) = month(now()) "+
// "and day(lastdatetime) = day(now()) "+
// "and  onlinestatus > 1 "+
// ") b on a.baht_ASK965 = b.mIN_baht_ASK965 "+
// "where year(a.lastdatetime) = year(now()) "+
// "and month(a.lastdatetime) = month(now()) "+
// "and day(a.lastdatetime) = day(now()) "+
// "and  a.onlinestatus > 1 "+
// "UNION ALL "+
// "select 'BID999' AS MAXTYPE,DATE_FORMAT(min(lastdatetime),'%H:%i:%s') AS MAX_TIME_BID999,MAX(BAHT_BID999) AS MAX_BAHT_BID999  from snp_bahtprice_hist a "+
// "inner join ( "+
// "select  max(Baht_bid999) as max_baht_bid999 from snp_bahtprice_hist "+
// "where year(lastdatetime) = year(now()) "+
// "and month(lastdatetime) = month(now()) "+
// "and day(lastdatetime) = day(now()) "+
// "and  onlinestatus > 1 "+
// ") b on a.baht_bid999 = b.max_baht_bid999 "+
// "where year(a.lastdatetime) = year(now()) "+
// "and month(a.lastdatetime) = month(now()) "+
// "and day(a.lastdatetime) = day(now()) "+
// "and  a.onlinestatus > 1 "+
// "UNION ALL "+
// "select  'ASK999' AS MAXTYPE,DATE_FORMAT(min(lastdatetime),'%H:%i:%s') AS MIN_TIME_ASK999 ,MIN(Baht_ask999 ) AS MIN_BAHT_ASK999  from snp_bahtprice_hist a "+
// "inner join ( "+
// "select  MIN(Baht_ASK999) as MIN_baht_ASK999 from snp_bahtprice_hist "+
// "where year(lastdatetime) = year(now()) "+
// "and month(lastdatetime) = month(now()) "+
// "and day(lastdatetime) = day(now()) "+
// "and  onlinestatus > 1 "+
// ") b on a.baht_ASK999 = b.mIN_baht_ASK999 "+
// "where year(a.lastdatetime) = year(now()) "+
// "and month(a.lastdatetime) = month(now()) "+
// "and day(a.lastdatetime) = day(now()) "+
// "and  a.onlinestatus > 1",
dashboard_spot_maxdaily : "select year(LastDateTime) as yr,month(LastDateTime) as mth,day(LastDateTime) as dy "+
",max(spotgold_bid) as max_spotbid,max(spotgold_offer) as max_spotask "+
"from snp_bahtprice_hist "+
"where LastDateTime between DATE_SUB(now(),INTERVAL 7 DAY) and now() "+
"group by year(LastDateTime),month(LastDateTime),day(LastDateTime)",
dashboard_AvgSpotByDay : "select HOUR(LastDateTime),AVG(spotgold_bid) as avg_spotbid "+
",AVG(spotgold_offer) as avg_spotoffer "+
"from snp_bahtprice_hist  "+
"where CONVERT(LastDateTime,date) = CURRENT_DATE() "+
"group by HOUR(LastDateTime) ",
SalesAndBuyAssessment : "select sum(case when oh.order_type = 'B' then order_qty else 0 end)  as sumorder_qty_customerbuy " +
",sum(case when oh.order_type = 'O' then (-1) * order_qty else 0 end)  as sumorder_qty_customersales " +
",sum((case when oh.order_type = 'O' then -1 else 1 end) * od.order_qty) as order_qty_remain " +
",sum(case when oh.order_type = 'B' then (order_unitprice) else 0 end ) / sum(case when oh.order_type = 'B' then 1 else 0 end ) as avg_unitPrice_customerbuy " +
",sum(case when oh.order_type = 'O' then (order_unitprice) else 0 end ) / sum(case when oh.order_type = 'O' then 1 else 0 end ) as avg_unitPrice_customersales " +
"from  snp_order_detail od " +
"inner join snp_order_header oh on od.order_no = oh.order_no " +
"where oh.order_type in('B','O') and oh.customer_code = ?",

};
