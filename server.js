import express from "express"
import bodyParser from "body-parser"
import jsonwebtoken from "jsonwebtoken"
import cors from "cors"
import configvalue from "./config"
//import mysql from "mysql";

const { port } = configvalue
const app = express()

app
  .use(bodyParser.urlencoded({ limit: "10mb", extended: true }))
  .use(bodyParser.json({ limit: "10mb", extended: true }))
  .use(cors())

app.get("/", (req, res) => {
  res.json({ message: "Welcome to bezkoder application." })
})

app.get("/api/", (req, res) => {
  res.send([])
})

// routes
require("./app/routes/tetet.routes")(app)
require("./app/routes/land.routes")(app)

require("./server_custom")(app)
app.listen(port, () => {
  console.log("Isomorphic JWT login " + port)
})
