module.exports = {
  HOST: "http://192.168.1.50",
  USER: "test",
  PASSWORD: "testpass",
  PORT: "20547",
  DB: "testDBName",
  dialect: "postgres",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
}
