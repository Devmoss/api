const db = require("../models")
const Op = db.Sequelize.Op

module.exports = {
  readByID: async (req, res) => {
    try {
      var schema = req.query.org_code
      const land = db.Land.schema(schema)
      const id = req.params.id
      const search = "%" + req.query.search + "%"
      const Data = await land
        .findAll
        // {
        // where: {
        // bid: { [Op.iLike]: search }
        // }
        // }
        ()
      return res.json(Data)
    } catch (e) {
      return res.status(500).json({ message: "Cannot get data from database." })
    }
  },
  readBy: async (req, res) => {
    try {
      var schema = req.query.org_code
      const land = db.Land.schema(schema)
      const search = "%" + req.query.search + "%"
      const Data = await land.findAll({
        where: {
          parcel_code: { [Op.iLike]: search },
        },
      })
      return res.json(Data)
    } catch (e) {
      return res
        .status(500)
        .json({ message: "Cannot get data from database." + e.message })
    }
  },
  create: async (req, res) => {
    var schema = req.body.org_code
    const land = db.Land.schema(schema)
    const data = req.body
    //console.log(data);
    if (data) {
      try {
        const result_data = await db.sequelize.transaction((t) => {
          return land.create(data, { transaction: t })
        })
        return res.status(201).json(result_data)
      } catch (e) {
        return res
          .status(500)
          .json({ message: e + "Cannot store data to database." })
      }
    }
    return res.status(400).json({ message: "Bad request." })
  },
  update: async (req, res) => {
    var schema = req.body.org_code
    const land = db.Land.schema(schema)
    const id = req.params.id
    const data = req.body
    console.log(id)
    if (id && data) {
      await db.sequelize.transaction((t) => {
        return land.update(data, { where: { id: id } }, { transaction: t })
      })
      return res.json(data)
    }
    return res.status(400).json({ message: "Bad request." })
  },
  delete: async (req, res) => {
    var schema = req.body.org_code
    const land = db.Land.schema(schema)
    const id = req.params.id
    if (id) {
      try {
        await land.destroy({ where: { id: id } })
        return res.status(204).send()
      } catch (e) {
        return res
          .status(500)
          .json({ message: "Cannot remove data from database." })
      }
    } else {
      return res.status(400).json({ message: "Bad request." })
    }
  },
}
