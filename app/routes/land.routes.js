const controller = require("../controllers/land.controller")

module.exports = function (app) {
  app.get("/api/land/:id", controller.readByID)
  app.get("/api/land", controller.readBy)
  app.post("/api/land", controller.create)
  app.put("/api/land/:id", controller.update)
  app.delete("/api/land/:id", controller.delete)
}
