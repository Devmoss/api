const controller = require("../controllers/tetet.controller")

module.exports = function (app) {
  app.get("/api/tetet/:id", controller.readByID)
  app.get("/api/tetet", controller.readBy)
  app.post("/api/tetet", controller.create)
  app.put("/api/tetet/:id", controller.update)
  app.delete("/api/tetet/:id", controller.delete)
}
