const config = require("../config/db.config.js")
const Sequelize = require("sequelize")
const sequelize = new Sequelize(config.DB, config.USER, config.PASSWORD, {
  host: config.HOST,
  port: config.PORT,
  dialect: config.dialect,
  operatorsAliases: false,
  pool: {
    max: config.pool.max,
    min: config.pool.min,
    acquire: config.pool.acquire,
    idle: config.pool.idle,
  },
})

const db = {}
db.Sequelize = Sequelize
db.sequelize = sequelize
//======== list required model ===========
db.Tetet = require("../models/tetet.model.js")(sequelize, Sequelize)
db.Land = require("../models/land.model.js")(sequelize, Sequelize)
//========================================
db.building.sche
db.ROLES = ["user", "admin", "moderator"]

module.exports = db
