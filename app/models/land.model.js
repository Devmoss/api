module.exports = (sequelize, Sequelize) => {
  const land = sequelize.define(
    "land",
    {
      id: { type: Sequelize.INTEGER, primaryKey: true },
      create_at: { type: Sequelize.DATE },
      update_at: { type: Sequelize.DATE },
    },
    {
      timestamps: false,
      freezeTableName: true,
    }
  )
  return land
}
