module.exports = (sequelize, Sequelize) => {
  const tetet = sequelize.define(
    "tetet",
    {
      id: { type: Sequelize.INTEGER, primaryKey: true },
      create_at: { type: Sequelize.DATE },
      update_at: { type: Sequelize.DATE },
      dfsds: { type: Sequelize.STRING },
      esfrds: { type: Sequelize.STRING },
    },
    {
      timestamps: false,
      freezeTableName: true,
    }
  )
  return tetet
}
